import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';
import {Reporter, Severity} from "jest-allure/dist/Reporter";

declare const reporter: Reporter;
const cats: CatMinInfo[] = [{ name: 'Тестовыйкот', description: '', gender: 'male' }];
const catDescription = 'Тестовое описание кота Смартфон Vivo';
let catId;
const HttpClient = Client.getInstance();

describe('API Core - save-description', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не удалось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });
  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });
 
  it('Добавление описания котику', async () => {
    reporter.severity(Severity.Minor);
    reporter.description('Тест на добавление описания котику');
    reporter.startStep('Запрос на добавление описания коту по id');
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        catId: catId,
        catDescription: catDescription,
      },
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();
    reporter.startStep('Проверка статуса и структуры ответа');
    expect(response.statusCode).toEqual(200);
    expect(response.body).toMatchObject({
      id: catId,
      description: catDescription,
    });
    reporter.endStep();
  });
});
