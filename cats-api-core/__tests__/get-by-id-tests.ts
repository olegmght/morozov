import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';
import {Reporter, Severity} from "jest-allure/dist/Reporter";

declare const reporter: Reporter;
const cats: CatMinInfo[] = [{ name: 'Тестовыйкотик', description: '', gender: 'male' }];
let catId;
const fakeId = 'fakeId';
const HttpClient = Client.getInstance();

describe('API Core - get-by-id', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не удалось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });
  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });
  
  it('get-by-id - Поиск по существующему id ', async () => {
    reporter.severity(Severity.Critical);
    reporter.description('Тест на поиск по существующему id');
    reporter.startStep('Запрос на поиск кота по его ID');
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();
    reporter.startStep('Проверка статуса и структуры ответа');
    expect(response.statusCode).toEqual(200);
    expect(response.body).toEqual({cat: {
      id: catId,
      ...cats[0],
      tags: null,
      likes: 0,
      dislikes: 0,
    }});
    reporter.endStep();
  });

  it('get-by-id - Поиск по некорректному id', async () => {
    reporter.severity(Severity.Normal);
    reporter.description('Тест на поиск по некорректному id');
    reporter.startStep('Проверка статус-кода ответа');
    await expect(
      HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
      })
    ).rejects.toThrowError('Response code 400 (Bad Request)');
    reporter.endStep();
  });
});
