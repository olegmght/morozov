const catsStorage = require('./storage');

/**
 * Группировка и сортировка полученных котов с характеристиками из БД
 * @param {*} cats - список строк котов с характеристиками, которые возвращаются клиенту
 */
function groupNamesAndSort(cats, reverseSort) {
  const groups = groupByFirstLetter(cats);
  const sorterGroup = sortGroupAlphabetically(groups, reverseSort);
  const count = countNames(sorterGroup);

  return {
    groups: sorterGroup,
    count,
  };
}

function groupNamesAndSortByLetter(cats, reverseSort) {
  const groups = groupByFirstLetter(cats);
  const sorterGroup = sortGroupAlphabetically(groups, reverseSort);
  const { countOutput, countAll } = countNamesByLetter(sorterGroup);

  return {
    groups: sorterGroup,
    count_output: countOutput,
    count_all: countAll,
  };
}
/**
 * Группировка котов (список объектов) по первой букве
 * @param {*} cats - список объектов котов с характеристиками, которые возвращаются клиенту
 */
function groupByFirstLetter(cats) {
  const groups = {};

  for (let i = 0; i < cats.length; i++) {
    const cat = cats[i];
    const name = cat.name;
    const title = name.charAt(0);

    if (groups[title] == null) {
      groups[title] = [cat];
    } else {
      groups[title].push(cat);
    }
  }

  return groups;
}
/**
 * Сортировка групп котов в алфавитном порядке
 * @param {*} groups - мапа групп готов, содержит title и список объектов
 */
function sortGroupAlphabetically(groups, reverseSort) {
  const collator = new Intl.Collator('ru');
  const keysSortedAlphabetically = Array.from(Object.keys(groups)).sort((a, b) => {
    return collator.compare(a, b);
  });
  const sorterGroup = [];

  if (reverseSort) {
    keysSortedAlphabetically.reverse();
  }

  for (let i = 0; i < keysSortedAlphabetically.length; i++) {
    const key = keysSortedAlphabetically[i];
    const group = {
      title: key,
      cats: groups[key].sort(function (a, b) {
        return collator.compare(a, b);
      }), // .sort()
    };
    sorterGroup.push(group);
  }

  return sorterGroup;
}
/**
 * Вычисление количества найденных имен в списке
 * @param {*} groups - список групп с именами
 */
function countNames(groups) {
  let count = 0;
  for (let i = 0; i < groups.length; i++) {
    const group = groups[i];
    group['count'] = group.cats.length;
    count = count + group.count;
  }
  return count;
}

function countNamesByLetter(groups) {
  let countOutput = 0;
  let countAll = 0;
  for (let i = 0; i < groups.length; i++) {
    const group = groups[i];
    group['count_in_group'] = Number(group.cats.length);
    group['count_by_letter'] = Number(group.cats[0].count_by_letter);
    countOutput += group.count_in_group;
    countAll += group.count_by_letter;
  }
  return { countOutput, countAll };
}

// eslint-disable-next-line require-jsdoc
function isEmpty(value) {
  return value == null || value.length === 0;
}

/**
 * Проверка, что id из заспроса - целое и положительное число
 * @param {string} id - пришедший в запросе id
 */
function isValidId(id) {
  // Используем Number(...) т.к. parseFloat/parseInt могут обработать валидный префикс в невалидной строке
  // Например parseFloat('1абс') = 1
  return Number.isInteger(parseFloat(id));
}

async function checkValidNameOfRules(name, validRules) {
  for (let i = 0; i < validRules.length; i++) {
    const { description, regex } = validRules[i];
    const validationRegex = new RegExp(regex);

    const isValid = name.search(validationRegex) > -1;
    if (isValid === false) {
      return description;
    }
  }
  return null;
}

async function checkValidAddCat(cat) {
  if (cat.gender === undefined || '') {
    return 'Заполните всю форму, не указан пол';
  }
  return await checkValidCatName(cat.name, 'add');
}

async function checkValidCatName(catName, rule) {
  if (catName == null || catName.length === 0) {
    return 'Имя не может быть пустым';
  } else if (catName.length > 35) {
    return 'Имя не может быть длиннее 35 символов';
  }
  const validRules = await catsStorage.findCatsValidationRules(rule);
  return await checkValidNameOfRules(catName, validRules);
}

module.exports = {
  groupNamesAndSort,
  isEmpty,
  isValidId,
  checkValidCatName,
  checkValidAddCat,
  groupNamesAndSortByLetter,
  countNamesByLetter,
};
